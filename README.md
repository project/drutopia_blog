<!-- writeme -->
Drutopia Blog
=============

Drutopia Blog is a base feature providing a blog content type and related configuration.

 * https://gitlab.com/drutopia/drutopia_blog
 * Issues: https://gitlab.com/drutopia/drutopia_blog/issues
 * Source code: https://gitlab.com/drutopia/drutopia_blog/tree/8.x-1.x
 * Keywords: blogging, journal, content, drutopia
 * Package name: drupal/drutopia_blog


### Requirements

 * drupal/block_visibility_groups ^1.3
 * drupal/config_actions ^1.1
 * drupal/ctools ^3.4
 * drupal/drutopia_seo ^1.0
 * drupal/ds ^3.7
 * drupal/facets ^2
 * drupal/field_group ^3.0
 * drupal/pathauto ^1.8
 * drupal/token ^1.7


### License

GPL-2.0+

<!-- endwriteme -->
